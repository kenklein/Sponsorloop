export interface Competition {
  name: string;
  description: string;
  targetAmountutv: number;
  actualAmountutv: number;
  targetAmounticb: number;
  actualAmounticb: number;
  targetAmountother: number;
  actualAmountother: number;
}
