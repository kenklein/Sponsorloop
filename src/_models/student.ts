export interface Student {
  name: string;
  team: string;
  description: string;
  targetAmount: number;
  actualAmount: number;
  amountRaised?: number;
  imageScr?: string;
}
