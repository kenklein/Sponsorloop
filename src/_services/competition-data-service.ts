import {Competition} from '../_models/competition';

export const COMPETITION_DATA: Competition[] = [
  {
    name: 'Competitie',
    description: 'Overzicht van de gelopen kilometers tussen de verenigingen (U.T.V. vs ICB) en overige deelnemers.',
    targetAmountutv: 75,
    actualAmountutv: 61.84,
    targetAmounticb: 75,
    actualAmounticb: 72.01,
    targetAmountother: 75,
    actualAmountother: 0,
  },
];
