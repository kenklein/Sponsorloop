import {Student} from '../_models/student';

export const STUDENT_DATA: Student[] = [
  {
    name: 'Wouter Slob',
    team: 'U.T.V. - Utrechtse Technische Vereniging',
    targetAmount: 4.4,
    actualAmount: 4.4,
    amountRaised: 147.29,
    description: 'team U.T.V.',
  },
  {
    name: 'Mark Smits',
    team: 'ICB - Ingenium Cabo Bianci',
    targetAmount: 11.60,
    actualAmount: 11.60,
    amountRaised: 400,
    description: 'Dispuut Whose Kangaroo',
    imageScr: 'assets/img/png/mark_smits.png',
  },
  {
    name: 'Koen Spruijt',
    team: 'U.T.V. - Utrechtse Technische Vereniging',
    targetAmount: 8.73,
    actualAmount: 8.73,
    amountRaised: 15,
    description: '',
  },
  {
    name: 'Louise Blom',
    team: 'U.T.V. - Utrechtse Technische Vereniging',
    targetAmount: 4.3,
    actualAmount: 4.3,
    amountRaised: 21.50,
    description: 'Team U.T.V. !!',
    imageScr: 'assets/img/png/louise_blom.png'
  },
  {
    name: 'Carl Beelhuizen',
    team: 'ICB - Ingenium Cabo Bianci',
    targetAmount: 16,
    actualAmount: 16,
    amountRaised: 16,
    description: '',
    imageScr: 'assets/img/png/carl_beelhuizen.png'
  },
  {
    name: 'Thom van Empelen',
    team: 'ICB - Ingenium Cabo Bianci',
    targetAmount: 9,
    actualAmount: 9,
    amountRaised: 36,
    description: 'Jaaajajajaajaja',
    imageScr: 'assets/img/png/thom_van_empelen.png'
  },
  {
    name: 'Milan Dammingh',
    team: 'ICB - Ingenium Cabo Bianci',
    targetAmount: 5.02,
    actualAmount: 5.02,
    amountRaised: 70,
    description: 'Ondanks dat ik niks meer kon na de 5 km, die ik heb gelopen, heb ik veel geld opgehaald om ervoor te zorgen dat we sneller met ze alle weer in de kroeg kunnen staan. We hoeven dan ook niet meer hard te lopen.',
    imageScr: 'assets/img/png/milan_dammingh.png',
  },

  {
    name: 'Floor Zandstra',
    team: 'U.T.V. - Utrechtse Technische Vereniging',
    targetAmount: 8.01,
    actualAmount: 8.01,
    amountRaised: 24.00,
    description: '',
    imageScr: 'assets/img/png/floor_zandstra.png',
  },

  {
    name: 'Cami Tak',
    team: 'U.T.V. - Utrechtse Technische Vereniging',
    targetAmount: 10.15,
    actualAmount: 10.15,
    amountRaised: 100,
    description: '',
    imageScr: 'assets/img/png/cami_tak.png',
  },

  {
    name: 'Machteld Menkveld',
    team: 'U.T.V. - Utrechtse Technische Vereniging',
    targetAmount: 13,
    actualAmount: 13,
    amountRaised: 13,
    description: ''
  },

  {
    name: 'Lars Goris',
    team: 'ICB - Ingenium Cabo Bianci',
    targetAmount: 5.01,
    actualAmount: 5.01,
    amountRaised: 5.00,
    description: 'Samen met huisgenoot gelopen.',
    imageScr: 'assets/img/png/lars_goris.png',
  },

  {
    name: 'Didier Wouters',
    team: 'ICB - Ingenium Cabo Bianci',
    targetAmount: 5.01,
    actualAmount: 5.01,
    amountRaised: 25.00,
    description: 'Samen met huisgenoot gelopen.',
    imageScr: 'assets/img/png/didier_wouters.png',
  },

  {
    name: 'Marciano Mascini',
    team: 'U.T.V. - Utrechtse Technische Vereniging ',
    targetAmount: 13.5,
    actualAmount: 13.5,
    amountRaised: 23,
    description: '',
    imageScr: 'assets/img/png/marciano_mascini.png',
  },
  {
    name: 'Tycho Bakker',
    team: 'ICB - Ingenium Cabo Bianci',
    targetAmount: 4.9,
    actualAmount: 4.9,
    amountRaised: 5,
    description: '',
    imageScr: 'assets/img/tycho_bakker.png',
  },
  {
    name: 'Maud van Loon',
    team: 'ICB - Ingenium Cabo Bianci',
    targetAmount: 8,
    actualAmount: 8,
    amountRaised: 20.00,
    description: '',
    imageScr: 'assets/img/png/maud_van_loon (1).png',
  },
  {
    name: 'Kelly Brabander',
    team: 'ICB - Ingenium Cabo Bianci',
    targetAmount: 5,
    actualAmount: 5,
    amountRaised: 7.50,
    description: '',
  },


];
